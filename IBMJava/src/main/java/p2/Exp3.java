package p2;

import p1.Exp1;

public class Exp3 extends Exp1{
	
	public Exp3()
	{
		System.out.println("constructor");//3
	}
	
	static
	{
		System.out.println("static");//1
		
		//dummy data
		//database
	}
	
	{
		System.out.println("instance");//2
	}
	
	public static void main(String[] args) {
		
		Exp3 obj = new Exp3();
		Exp3 obj1 = new Exp3();
		
		int x = 101;
		
		float j = 23.33f;
		
		double k = 345.484;
		
		char l = 'A';
		
		boolean m = true;
		
		
		
		System.out.println(x);
		System.out.println(j);
		System.out.println(k);
		System.out.println(l);
		System.out.println(m);
	}
}
