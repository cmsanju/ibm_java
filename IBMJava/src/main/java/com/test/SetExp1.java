package com.test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class SetExp1 {
	
	public static void main(String[] args) {
		
		Set data = new HashSet();
		
		data.add(10);//0
		data.add(100);//1
		data.add("java");//2
		data.add('A');//3
		data.add("java");//4
		data.add(200.44);//5
		data.add(33.43f);//6
		data.add(10);//7
		data.add(true);//8
		
		System.out.println(data);
		
		Set data1 = new LinkedHashSet();
		
		data1.add(10);//0
		data1.add(100);//1
		data1.add("java");//2
		data1.add('A');//3
		data1.add("java");//4
		data1.add(200.44);//5
		data1.add(33.43f);//6
		data1.add(10);//7
		data1.add(true);//8
		
		
		System.out.println(data1);
		
		
		Iterator itr = data1.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		System.out.println(data1.size());
		System.out.println(data1.isEmpty());
		
		data1.clear();
		
		System.out.println(data1.isEmpty());
	}
}
