package com.test;

import java.util.Scanner;

public class ExceptionsExp1 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try
		{
			System.out.println(30/3);
			
			//System.out.println(args[0]);
			
			String name = null;
			
			System.out.println(name.charAt(0));
		}
		//System.out.println();
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("enter the array input");
		}
		catch(NullPointerException npe)
		{
			System.out.println("please enter string input");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		finally
		{
			try {
				if(sc != null)
				{
					sc.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
	}
}
