package com.test;

class BookData
{
	int id = 303;
	
	public BookData()
	{
		System.out.println("Default");
	}
	
	public BookData(String name)
	{
		System.out.println("Book : "+name);
	}
}

class BookImpl extends BookData
{
	static String name = "Cloud notes";
	
	public BookImpl()
	{
		super(name);
		
		System.out.println("sub class constructor");
	}
	
	public void disp()
	{
		System.out.println("ID : "+super.id+" "+name);
	}
}

public class ConstructorsExp2 {
	
	public static void main(String[] args) {
		
		BookImpl obj = new BookImpl();
		
		obj.disp();
	}
}
