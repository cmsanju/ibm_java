package p1;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Java8FilterMethod {
	
	public static void main(String[] args) {
		
		LinkedList<Employee> listEmp = new LinkedList<>();
		
		listEmp.add(new Employee(101, "Rahul", 2345));
		listEmp.add(new Employee(501, "Rohit", 5252));
		listEmp.add(new Employee(301, "Kohli", 578));
		listEmp.add(new Employee(201, "Dravid", 532));
		listEmp.add(new Employee(401, "MSD", 922));
		listEmp.add(new Employee(201, "Dravid", 532));
		listEmp.add(new Employee(401, "MSD", 922));
		listEmp.add(new Employee(501, "Rohit", 5252));
		listEmp.add(new Employee(301, "Kohli", 578));
		
		List<Employee> fltEmp =  listEmp.stream()
				.filter(emp -> emp.getName().startsWith("R")).distinct()
		        .collect(Collectors.toList());
		
		fltEmp.forEach(obj-> System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getSalary()));
	}
}	
