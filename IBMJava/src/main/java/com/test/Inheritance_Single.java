package com.test;

class A // PARENT / SUPER / BASE
{
	int id = 101;
	String name = "King";
	
	public void userDetals()
	{
		System.out.println("ID : "+id+" Name : "+name);
	}
}
//IS-A, HAS-A, USES-A
class B extends A //CHILD / SUB / DERIVED
{
	String cmp = "E Y";
	
	public void empDetails()
	{
		System.out.println("ID : "+id+" Name : "+name+" City : "+cmp);
	}
}

class C extends B
{
	String city = "Blr";
	
	public void custDetails()
	{
		System.out.println("ID : "+id+" Name : "+name+" Company : "+cmp+" City : "+city);
	}
}

public class Inheritance_Single {
	
	
	
	public static void main(String[] args) {
		
		//B obj = new B();
		
		C obj = new C();
		
		obj.userDetals();
		obj.empDetails();
		obj.custDetails();
	}
}
