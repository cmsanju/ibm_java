package com.test;

class Book
{
	private int id;
	private String bookName;
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	//default constructor 
	public Book()
	{
		System.out.println("default");
	}

	//parameterised constructor
	public Book(int id, String bookName)
	{
		this.id = id;
		this.bookName = bookName;
	}
	
	//object parameterised constructor
	public Book(Book obj)
	{
		if(obj == null)
		{
			  obj = new Book();
		} 
	}
}//book class end

public class ConstructorsDemo {
	
	public static void main(String[] args) {
		
		Book b3 = new Book(101, "Java Notes");
		
		//Book b2 = new Book(201, "Cloud Computing");
		
		Book b1 = new Book(b3);
		
		b1.setId(101);
		b1.setBookName("Selenium");
		
		Book b2 = new Book();
		
		b2.setId(301);
		b2.setBookName("Automation");
		
		System.out.println(b1.getId()+" "+b1.getBookName());
		System.out.println(b2.getId()+" "+b2.getBookName());
		
	}
}
