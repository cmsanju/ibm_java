package com.test;

public class CheckVoterAge {
	
	public void checkAge(int age)throws ValidAgeException,InvalidAgeException
	{
		if(age >= 18)
		{
			ValidAgeException ve = new ValidAgeException("valid age");
			
			throw(ve);
		}
		else
		{
			InvalidAgeException ie = new InvalidAgeException("invalid age");
			
			throw(ie);
		}
	}
}
