package com.test;

public class DiamondPattern {
	
	// Main driver method
    public static void main(String[] args)
    {
 
        // Declaring and initialising variables
 
        // Variable initialised to the row where max star
        // should be there as after that they decreases to
        // give diamond pattern
        int number = 7;
 
        int m, n;
 
        // Outer loop 1
        // prints the first half diamond
        for (m = 1; m <= number; m++) {
 
            // Inner loop 1 print white spaces in between
            for (n = 1; n <= number - m; n++) {
                System.out.print(" ");
            }
 
            // Inner loop 2 prints star
            for (n = 1; n <= m * 2 - 1; n++) {
                System.out.print("*");
            }
 
            // Ending line after each row
            System.out.println();
        }
 
        // Outer loop 2
        // Prints the second half diamond
        for (m = number - 1; m > 0; m--) {
 
            // Inner loop 1 print white spaces in between
            for (n = 1; n <= number - m; n++) {
                System.out.print(" ");
            }
 
            // Inner loop 2 print star
            for (n = 1; n <= m * 2 - 1; n++) {
                System.out.print("*");
            }
 
            // Ending line after each row
            System.out.println();
        }
    }
}
/*	   *       (1 space, 1 asterisk)
      ***      (2 spaces, 3 asterisks)
     *****     (3 spaces, 5 asterisks)
    *******    (4 spaces, 7 asterisks)
   *********   (5 spaces, 9 asterisks)
  ***********  (6 spaces, 11 asterisks)
 ************* (7 spaces, 13 asterisks)
  ***********  (6 spaces, 11 asterisks)
   *********   (5 spaces, 9 asterisks)
    *******    (4 spaces, 7 asterisks)
     *****     (3 spaces, 5 asterisks)
      ***      (2 spaces, 3 asterisks)
       *       (1 space, 1 asterisk)



 */
