package com.test;

public class ImmutableString {
	
	public static void main(String[] args) {
		
		String str1 = "Hello";
		
		str1.concat(" Hi...");
		
		
		System.out.println(str1);
		
		StringBuffer sb = new StringBuffer(str1);//ac .no hold
		
		System.out.println(sb);
		
		sb.append(" Hi...");
		
		System.out.println(sb);
		
		StringBuffer sb2 = new StringBuffer("java");//
		
		System.out.println(sb.reverse());
		
		StringBuilder sbr = new StringBuilder(str1);
		
		sbr.append(" Java");
		
		System.out.println(sbr);
	}
}
