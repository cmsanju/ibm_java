package com.test;

public class ConditionalStatements {
	
	public static void main(String[] args) {
		
		//if block
		//if else
		//else if ladder
		//nested if blocks
		
		
		int age = 21;
		
		if(age == 18)
		{
			System.out.println("just eligible");
		}
		else if(age > 18)
		{
			System.out.println("eligible");
		}
		else
		{
			System.out.println("not eligible");
		}
		
	}
}
