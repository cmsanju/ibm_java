package com.test;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeSerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("student.txt");
		
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Student obj =(Student)ois.readObject();
		
		System.out.println(obj.id+" "+obj.name+" "+obj.city+" "+obj.pinCode);
		
		Student obj1 = new Student();
		
		System.out.println(obj1.pinCode);
	}
}
