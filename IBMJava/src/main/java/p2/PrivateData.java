package p2;

import java.lang.reflect.Method;

class Solution
{
	private void showMessage()
	{
		System.out.println("hello java"); 
	} 
}

class Solution1
{
	private void cube(int n)
	{
		System.out.println(n*n*n);
		
	}  
}

public class PrivateData {
	
	
	public static void main(String[] args)throws Exception{  
		  
	    Class c = Class.forName("p2.Solution");  
	    Object o= c.newInstance();  
	    Method m =c.getDeclaredMethod("showMessage", null);  
	    m.setAccessible(true);  
	    m.invoke(o, null);  
	    
	    
	    Class c1 = p2.Solution1.class;  
	    Object obj=c1.newInstance();  
	      
	    Method m1 =c1.getDeclaredMethod("cube",new Class[]{int.class});  
	    m1.setAccessible(true);  
	    m1.invoke(obj,4);  
	}  
}
