package com.test;

import java.util.Scanner;

public class TestVoter {
	
	public static void main(String[] args) //throws ValidAgeException,InvalidAgeException
	{
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter age");
		
		int age = sc.nextInt();
		
		CheckVoterAge obj = new CheckVoterAge();
		
		try {
			obj.checkAge(age);
		}
		catch(ValidAgeException ve)
		{
			System.out.println("eligible");
		}
		catch (InvalidAgeException ie) {
			System.out.println("not eligible");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}
