package com.test;

public class OperatorsJava {
	
	public static void main(String[] args) {
		
		long j = 200;
		
		long k = 400;
		
		System.out.println(j+k);
		
		System.out.println(j-k);
		
		System.out.println(j*k);
		
		System.out.println(j/k);
		
		System.out.println(j%k);
		
		int x = 5;
		
		if(x%2 == 0)
		{
			System.out.println("even");
		}
		else
		{
			System.out.println("odd");
		}
	}
}
