package com.test;

interface Inf1
{
	int x = 101;
	
	 void message();
	
	default void greet()
	{
		System.out.println("Hi hello.");
	}
	
	static void fox()
	{
		System.out.println("fox() static");
	}
}

abstract class Abs 
{
	public abstract void dog();
	
	public void cat()
	{
		System.out.println("cat() abs");
	}
	
}

class Impl extends Abs implements Inf1
{
	@Override
	public void dog()
	{
		System.out.println("dog() overrided");
	}
	
	@Override
	public void message()
	{
		System.out.println("inf1 overrided");
	}
}


public class MultipleExp {
	
	public static void main(String[] args) {
		
		//Inf1 obj = new Inf1();
		//Abs obj = new Abs();
		
		Impl obj = new Impl();
		
		obj.dog();
		obj.message();
		obj.greet();
		obj.cat();
		
		Inf1.fox();
		
	}

}
