package com.test;

class Simple_Methods
{
	//type 1 method
	
	public int methodType1(int x, String str)
	{
		
		System.out.println("type 1 method");
		
		return x;
	}
	
	//type 2 method
	/* heading */        /*signature*/
	public void methodType2()
	{
		// body
		System.out.println("type 2 method");
	}
	
	//type 3 method
	
	public void methodType3(String name)
	{
		System.out.println("TYPE 3 METHOD");
	}
	
	//type 4 method
	public String methodType4()
	{
		System.out.println("type 4 method");
		
		return "java";
	}
}

public class MethodsExp {
	
	public static void main(String[] args) {
		
		Simple_Methods obj = new Simple_Methods();
		
		int y = obj.methodType1(7, "ibm");
		obj.methodType2();
		obj.methodType3("hello");
		String str = obj.methodType4();
		
		System.out.println(y);
		System.out.println(str);
	}
}
