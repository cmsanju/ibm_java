package com.test;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetExp1 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data1 = new TreeSet<>();
		
		data1.add(10);
		data1.add(30);
		data1.add(50);
		data1.add(4);
		data1.add(7);
		data1.add(2);
		data1.add(5);
		data1.add(11);
		
		Iterator<Integer> itr = data1.descendingIterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}
}
