package p1;

public class Exp1 {
	
	private int a = 101;
	
	        int b = 303;
	        
	protected int c = 23;
	
	public int d = 404;
	
	static int pCode = 123123;
	
	public void dispData()
	{
		int x = 34;
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
	}
	
	public static void show()
	{
		System.out.println("STATIC METHOD");
		
	}
	
	public static void main(String[] args) {
		
		System.out.println("main()");
		
		Exp1 obj = new Exp1();
		
		System.out.println(obj.a);
		System.out.println(obj.b);
		
		obj.dispData();
		
		Exp1.show();
		
		System.out.println(Exp1.pCode);
		
		//int -> Integer
		
		//JVM dependent //byte code machine level .java -> .class 
	}
}

class Exp11 extends Exp1
{
	@Override
	public void dispData()
	{
		//System.out.println(a);
		
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		
	}
	
}
