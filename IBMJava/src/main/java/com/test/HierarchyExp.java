package com.test;

class Parent
{
	public void draw()
	{
		System.out.println("circle");
	}
}


class Child1 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("Square");
	}
	
	public void cat()//1 year back addr  10 new address
	{
		
	}
	
	//public abstract void dog();
}

class Child2 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("Triangle");
	}
}

public class HierarchyExp {
	
	public static void main(String[] args) {
		
		Parent obj = new Parent();
		
		obj.draw();
		
		Parent obj1 = new Child1();//
		
		obj1.draw();
		
		//obj1.cat();
		
		Parent obj2 = new Child2();
		
		obj2.draw();
		
	}
}
