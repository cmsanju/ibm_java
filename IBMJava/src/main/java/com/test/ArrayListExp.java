package com.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArrayListExp {
	
	public static void main(String[] args) {
		
		//Collection data = new ArrayList();
		//List data = new ArrayList();
		
		ArrayList data = new ArrayList();
		
		data.add(10);
		data.add(100);
		data.add("java");
		data.add('A');
		data.add("java");
		data.add(200.44);
		data.add(33.43f);
		data.add(10);
		data.add(true);
		
		System.out.println(data);
		
		System.out.println(data.size());//9
		
		System.out.println(data.get(7));
		
		System.out.println(data.contains(10));
		
		System.out.println(data.indexOf(1000));
		
		data.set(7, 300);//updating the index value
		
		System.out.println(data);
		
		data.remove(7);//for deleting the data from CFV
		
		System.out.println(data.size());
		
		System.out.println(data);
		
		System.out.println(data.subList(4, 7));
	}
}
