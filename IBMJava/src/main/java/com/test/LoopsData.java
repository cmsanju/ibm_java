package com.test;

public class LoopsData {
	
	public static void main(String[] args) {
		
		//for loop
		for(int i = 10; i >= 0; i--)
		{
			System.out.println(i);
			
			if(i == 5)
			{
				break;
			}
		}
		//System.exit(0);
		//while loop
		
		int x = 20;
		
		while(x <= 30)
		{
			System.out.println(x);
			
			x++;
		}
		
		System.out.println(x);
		
		int y = 50;
		
		do
		{
			System.out.println(y);
			
			y--;
		}
		while(y >= 40);
	}

}
