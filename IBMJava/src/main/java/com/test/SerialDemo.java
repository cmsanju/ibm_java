package com.test;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fos = new FileOutputStream("student.txt");
		
		ObjectOutputStream obj = new ObjectOutputStream(fos);
		
		Student std = new Student();
		
		obj.writeObject(std);
		
		System.out.println("Done.");
		
		
	}
}
