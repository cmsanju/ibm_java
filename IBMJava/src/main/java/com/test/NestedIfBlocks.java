package com.test;

public class NestedIfBlocks {
	
	public static void main(String[] args) {
		
		int ht = 173;
		
		int wt = 60;
		
		int age = 22;
		
		if(ht >= 170)
		{
			if(wt >= 55 && wt <= 65)
			{
				if(age >= 21 && age <= 30)
				{
					System.out.println("Success.");
				}
				else
				{
					System.out.println("age is not amtching");
				}
			}
			else
			{
				System.out.println("weight not matching");
			}
		}
		else
		{
			System.out.println("hight not matching");
		}
	}
}
