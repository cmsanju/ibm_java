package com.test;

public class StringExp {
	
	public static void main(String[] args) {
		
		String str1 = new String("Hello");
		String str2 = new String("hello");
		
		String str3 = "Hello";
		String str4 = "Hello";
		
		System.out.println(str1 == str2);//false
		System.out.println(str1.equals(str2));//false  
		
		System.out.println(str3 == str4);//true
		
		System.out.println(str1 == str3);//false
		
		System.out.println(str3.equals(str1));//true
		System.out.println(str2.equals(str4));//
		System.out.println(str1 == str4);
		
	}
}
