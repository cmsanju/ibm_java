package com.test;

//constructor chaining

class Customer
{
	private int id;
	
	private String name;
	
	
	public Customer()// this and this()
	{
		this(101);//2
		System.out.println("Default");
		
		return;
	}
	
	public Customer(int id)//execute
	{
		this("java");//3
		System.out.println("single int arg");
		
	}
	
	public Customer(String name)//execute
	{
		this(201, "JS");//4
		System.out.println("Single strin arg");
	}
	
	public Customer(int id, String name)//execute
	{
		System.out.println("double arg");
	}
}

public class ConstructorsDemo2 {
	
	public static void main(String[] args) {
		
		Customer obj1 = new Customer(85);//(4:55)
		
		
	}

}
