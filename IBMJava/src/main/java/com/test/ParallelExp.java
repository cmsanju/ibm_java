package com.test;

interface Inf2
{
	void greetUser();
}

interface Inf3 //extends Inf2
{
	void animal();
}

abstract class Abs1
{
	public abstract void dog();
}

class Impl1 extends Abs1 implements Inf2,Inf3
{

	@Override
	public void animal() {
		
		System.out.println("inf3 overrided");
		
	}

	@Override
	public void greetUser() {
		
		System.out.println("inf2 overrided");
	}

	@Override
	public void dog() {
		
		System.out.println("abs overrided");
	}
	
}

public class ParallelExp {
	
	public static void main(String[] args) {
		Impl1 obj = new Impl1();
		
		obj.dog();
		obj.greetUser();
		obj.animal();
	}
}
