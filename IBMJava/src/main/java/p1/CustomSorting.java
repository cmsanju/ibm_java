package p1;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class Customer implements Comparable<Customer>{
	
	private int id;
	
	private String name;
	
	private int salary;
	
	public Customer()
	{
		
	}
	
	public Customer(int id, String name, int salary)
	{
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
 /*
	@Override
	public int compareTo(Customer o) {
		
		return o.id-this.id;
	}
	*/
	
	@Override
	public int compareTo(Customer o)
	{
		return o.salary-this.salary;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, salary);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		return id == other.id && Objects.equals(name, other.name) && salary == other.salary;
	}
	
	
}

class NameSorting implements Comparator<Customer>
{

	@Override
	public int compare(Customer o1, Customer o2) {
		
		return o1.getName().compareTo(o2.getName());
	}
	
}

public class CustomSorting {
	
	public static void main(String[] args) {
		
		LinkedList<Customer> listCustomers = new LinkedList<>();
		
		listCustomers.add(new Customer(101, "Rahul", 2345));
		listCustomers.add(new Customer(501, "Rohit", 5252));
		listCustomers.add(new Customer(301, "Kohli", 578));
		listCustomers.add(new Customer(201, "Dravid", 532));
		listCustomers.add(new Customer(401, "MSD", 922));
		listCustomers.add(new Customer(201, "Dravid", 532));
		listCustomers.add(new Customer(401, "MSD", 922));
		listCustomers.add(new Customer(501, "Rohit", 5252));
		listCustomers.add(new Customer(301, "Kohli", 578));
		
		/*
		for(Customer cst : listCustomers)
		{
			System.out.println(cst.getId()+" "+cst.getName()+" "+cst.getSalary());
		}
		
		System.out.println("========SALARY SORTING=========");
		
		//comparable sorting
		Collections.sort(listCustomers);
		
		for(Customer cst : listCustomers)
		{
			System.out.println(cst.getId()+" "+cst.getName()+" "+cst.getSalary());
		}
		
		//comaparator sorting
		Collections.sort(listCustomers, new NameSorting());
		
		System.out.println("=======NAME SORTING==========");
		for(Customer cst : listCustomers)
		{
			System.out.println(cst.getId()+" "+cst.getName()+" "+cst.getSalary());
		}
		
		System.out.println("======java 8 new feature");
		//JAVA 8 NEW FEATURE
		
		listCustomers.stream()
		.sorted((obj1,obj2)-> (int)(obj2.getSalary()-obj1.getSalary()))
		.forEach(obj -> System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getSalary()));
	
		
		
		//java 8 asc order and dsc and name and salary comparing 
		listCustomers.stream()
		.sorted(Comparator.comparing(Customer::getName))
		.forEach(obj -> System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getSalary()));
		System.out.println("=====reverse order====");
		listCustomers.stream()
		.sorted(Comparator.comparing(Customer::getName).reversed())
		.forEach(obj -> System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getSalary()));
		
		System.out.println("=====name and salary sorting=====");
		
		listCustomers.stream()
		.sorted(Comparator.comparing(Customer::getName).thenComparing(Customer::getSalary))
		.forEach(obj -> System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getSalary()));
		*/
		//java 8 filter method
		
		List<Customer> fltNames = listCustomers.stream()
		.filter(obj -> obj.getName().startsWith("K")).distinct()
		.collect(Collectors.toList());
		
		
		fltNames.forEach(obj -> System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getSalary()));
	}
}
