package com.test;

public class MethodOverloading {
	
	public void add(int x)
	{
		System.out.println("single arg");
	}
	
	public void add(float x)
	{
		System.out.println("type of the arg");
	}
	
	public void add(int x, float y)
	{
		System.out.println("no of the args");
	}
	
	public void add(float x, int y)
	{
		System.out.println("order of the args");
	}
	
	public static void main(String[] args) {
		
		MethodOverloading obj = new MethodOverloading();
		
		obj.add(101);
		obj.add(45.5f);
		obj.add(94, 33.33f);
		obj.add(74.44f, 6447);
	}
}
