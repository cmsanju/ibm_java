package com.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Vector;

public class ArrayListExp2 {
	
	public static void main(String[] args) {
		
		//ArrayList data = new ArrayList();
		
		//Vector data = new Vector();
		
		LinkedList data = new LinkedList();
		
		data.add(10);//0
		data.add(100);//1
		data.add("java");//2
		data.add('A');//3
		data.add("java");//4
		data.add(200.44);//5
		data.add(33.43f);//6
		data.add(10);//7
		data.add(true);//8
		
		//Iterator -> ListIterator(only List objects) Enumeration 1.0v
		//1 hasNext() 2 next(), 3 remove() hasPrevious() previous()
		
		//Iterator itr = data.iterator();
		
		ListIterator ltr = data.listIterator();
		
		while(ltr.hasNext())
		{
			System.out.println(ltr.next());
		}
		
		while(ltr.hasPrevious())
		{
			System.out.println(ltr.previous());
		}
	}
}
