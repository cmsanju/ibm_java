package com.test;

import java.util.ArrayList;

public class OnlyNumInString {
	
	// Function to check if a string
    // contains only digits
    public static boolean onlyDigits(String str, int n)
    {
        String num="0123456789";
        ArrayList<Character> numbers = new ArrayList<Character>();
        for(int i=0;i<num.length();i++)
        {
            numbers.add(num.charAt(i));
        }
        // Traverse the string from
        // start to end
        for (int i = 0; i < n; i++) {
 
            // Check if character is
            // not a digit between 0-9
            // then return false
            if (!numbers.contains(str.charAt(i))) {
                return false;
            }
        }
        // If we reach here, that means
        // all characters were digits.
        return true;
    }
 
    // Driver Code
    public static void main(String args[])
    {
        // Given string str
        String str = "1a234";
        int len = str.length();
 
        // Function Call
        System.out.println(onlyDigits(str, len));
    }
}
