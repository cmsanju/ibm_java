package com.test;

public class UnaryOperators {
	
	
	public static void main(String[] args) {
		
		//unary operators i++, i-- | ++i, --i
		
		int a = 1;
		
		System.out.println(a++);//1
		System.out.println(a);//2
		
		System.out.println(a--);//2
		System.out.println(a);
		
		
		System.out.println(++a);//
		System.out.println(--a);
	}
}
