package com.test;

import java.io.Serializable;

public class Student implements Serializable
{
	
	private static final long serialVersionUID = 1L;

	public int id = 101;
	
	public String name = "Rohit";
	
	public String city = "Blr";
	
	public transient int pinCode = 123123;
	
}	
// transient and volatile