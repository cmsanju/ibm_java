package p2;

class Test{
	
}

public class ReflectionExp {
	
	void printName(Object obj){  
		  Class c=obj.getClass();    
		  System.out.println(c.getName());  
		  }  

	public static void main(String args[]) throws Exception {
		Class c = Class.forName("p2.Test");//class name
		System.out.println(c.getName());
		
		ReflectionExp obj1 = new ReflectionExp();
		
		Test obj2 = new Test();
		
		obj1.printName(obj2);
	}
}
