package com.test;

class Calculator
{
	public  int add(int x, int y)
	{
		return x+y;
	}
	
	public  int sub(int x, int y)
	{
		return x-y;
	}
	
	public  int mul(int x, int y)
	{
		return x*y;
	}
}

public class SwitchCase {
	
	public static void main(String[] args) {
		Calculator obj = new Calculator();
		int opt = 1;
		
		switch(opt)
		{
		case 1: 
			{
				
				System.out.println(obj.add(30,40));
			}
			//break;
		case 2:
			{
				System.out.println(obj.sub(90, 30));
			}
			break;
			
		case 3:
			{
				System.out.println(obj.mul(49, 2));
			}
			break;
			default : System.out.println("please select the option");
		}
	}
}
