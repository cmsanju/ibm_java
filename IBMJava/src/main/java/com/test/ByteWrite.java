package com.test;

import java.io.File;
import java.io.FileOutputStream;

public class ByteWrite {
	
	public static void main(String[] args) throws Exception
	{
		
		//File file = new File("src/sample.txt");
		
		FileOutputStream fos = new FileOutputStream("src/sample.txt");
		
		String msg = "this is simple byte stream write operation";
		
		fos.write(msg.getBytes());
		
		System.out.println("Done.");
	}
}
