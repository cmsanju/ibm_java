package com.test;

public class CountStringNum {
	
	public static void main(String arg[]) {
	    // enter string u want here.
	    String x[] = {
	      "Raj",
	      "77",
	      "101",
	      "99",
	      "Jio"
	    };
	    int cn = 0, cs = 0;

	    //print array elements
	    System.out.println("Array elements are: ");
	    for (int i = 0; i < x.length; i++) {
	      System.out.println(x[i]);
	    }

	    // scan the string.
	    for (int i = 0; i < x.length; i++) {
	      try {
	        int j = Integer.parseInt(x[i]);
	        cn++;
	      } catch (NumberFormatException e) {
	        cs++;
	      }
	    }
	    // show the numeric and string value after counting.
	    System.out.println("Numeric:" + cn + "\nStrings:" + cs);
	  }
}
