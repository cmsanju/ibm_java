package com.test;

public class UnknownExceptions {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(20/0);
		}
		catch(Exception e)
		{
			//using getMessage();
			System.out.println(e.getMessage());
			
			//printing the exception class object
			System.out.println(e);
			
			//using printStackTrace()
			
			e.printStackTrace();
		}
	}
}
