package com.test;

import java.util.*;
import java.util.Map.Entry;

public class MapExp1 {
	
	public static void main(String[] args) {
		
		HashMap<Integer, String> data = new  HashMap<>();
		
		data.put(101, "rahul");
		data.put(201, "Rohit");
		data.put(301, "King kohli");
		data.put(401, "dravid");
		data.put(101, "Jaiswal");
		data.put(301, "singh");
		data.put(501, "aswin");
		data.put(601, "aswin");
		data.put(101, "rahul");
		
		System.out.println(data);
		
		System.out.println(data.get(301));
		
		
		Iterator<Entry<Integer, String>> itr = data.entrySet().iterator();
		
		while(itr.hasNext())
		{
			Entry<Integer, String> et = itr.next();
			
			System.out.println(et.getKey()+" "+et.getValue());
		}
		
		for(Integer key : data.keySet())
		{
			System.out.println("Key : "+key+" Value : "+data.get(key));
		}
		
		data.forEach((k,v)->System.out.println(k+" "+v));
	}
}
// HashSet : random HashMap
// LinkedHashSet : insertion order LinkedHashMap
// TreeSet : sorting asc order TreeMap based on keys
