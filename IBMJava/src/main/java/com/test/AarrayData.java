package com.test;

public class AarrayData {
	
	public static void main(String[] args) {
		/*
		int[] ar = {100,20,30,400};//old data 
		
		ar[0] = 100;//new data
		ar[3] = 500;//new data
		
		int[] arr = new int[5];//0-4
		
		arr[0] = 10;
		arr[1] = 30;
		arr[2] = 40;
		arr[3] = 50;
		arr[4] = 60;
		
		//System.out.println(arr[4]);
		
		for(int i = 0; i < arr.length; i++)
		{
			System.out.println(arr[i]);
		}
		
		String[] ar1 = {"java","hi", "hello", "test"};
		*/
		
		int[][] arr = {{10,20,30}, {40,50,60},{70,80,90}};
		
		for(int i = 0; i < arr.length; i++)//r
		{
			for(int j = 0; j < arr.length; j++)//c
			{
				System.out.print(arr[i][j]+" ");//10 20 30
			}
			System.out.println();
		}
	}
}
